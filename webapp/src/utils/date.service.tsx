import * as moment from 'moment';

export class DateUtils {
  public static getDaysUntilMonthEnd(mmt) {
    return DateUtils.getDiffInDays(moment.utc(mmt).endOf('month'), mmt);
  }

  public static getDiffInDays(mmt1, mmt2) {
    return moment.utc(mmt1).diff(moment.utc(mmt2), 'days');
  }

  public static isEndOfMonth(mmt: string): boolean {
    // startOf allows to ignore the time component
    // we call moment(mmt) because startOf and endOf mutate the momentj object.
    return moment
      .utc(mmt)
      .startOf('day')
      .isSame(
        moment
          .utc(mmt)
          .endOf('month')
          .startOf('day')
      );
  }

  public static getFormatedDate(date: string | moment.Moment, format: string): string {
    return moment.utc(date).format(format);
  }

  public static getLastDateOfTheMonth(date: string): moment.Moment {
    return moment.utc(date).endOf('month');
  }
}
