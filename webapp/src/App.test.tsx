import * as React from 'react';
import 'jest';
import { MeterReadings } from './components/meter-readings/meter-readings.interface';
import { MeterReadingService } from './components/meter-readings/meter-reading.service';
import * as TestRenderer from 'react-test-renderer';
import App from './App';
import * as moment from 'moment';

//Can write more tests,  but all of them are covered in the components.
describe('App', () => {
  let data: Array<MeterReadings>;
  let mockMeterReadingData;
  beforeEach(() => {
    mockMeterReadingData = {
      electricity: [
        {
          cumulative: 18905,
          readingDate: '2017-10-27T00:00:00.000Z',
          unit: 'kWh'
        },
        {
          cumulative: 19150,
          readingDate: '2017-11-04T00:00:00.000Z',
          unit: 'kWh'
        }
      ]
    };

    data = mockMeterReadingData.electricity.map((reading: MeterReadings) => {
      return {
        ...reading,
        readingDateDisplay: moment
          .utc(reading.readingDate)
          .format(MeterReadingService.DATE_FORMAT)
      };
    });
    jest.spyOn(MeterReadingService, 'getMeterReadings').mockResolvedValue(data);
  });

  it('should render', () => {
    const tableRendrer = TestRenderer.create(<App />);
    const tree = tableRendrer.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
