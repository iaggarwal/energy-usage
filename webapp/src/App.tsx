import * as React from 'react';
import { MeterReadingsComponent } from './components/meter-readings/meter-readings.component';
import { AppWrapper } from './components/styles/app-wrapper.component';

export default () => {

  return (
    <AppWrapper>
      <MeterReadingsComponent/>
    </AppWrapper>
  );
};
