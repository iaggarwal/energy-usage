export interface MeterReadingsResponse {
    electricity: Array<MeterReadings>;
}

export interface MeterReadings {
    cumulative: number;
    readingDate: string;
    readingDateDisplay: string;
    unit: string;
}

export interface State {
    loading: boolean;
    hasError: boolean;
    meterReadings: Array<MeterReadings>;
    energyUsage: Array<EnergyUage>;
}

export interface EnergyUage {
    date: string;
    usage: number;
}
