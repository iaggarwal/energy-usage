import styled from 'styled-components';
import { Card } from '../styles/card.component';

export const MeterReadingsWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 15px;
`;

export const MeterReadingItems = styled(Card)`
    flex: 1 1 auto;
    border: 1px solid;
    box-shadow: 5px 10px 8px #888888;
`


export const TableWrapper = styled(MeterReadingItems)`
    margin-top: 15px;
    table {
        width: 800px;
    }
`;

export const BarChartWrapper = styled(MeterReadingItems)`
    margin-bottom: 15px;
`
