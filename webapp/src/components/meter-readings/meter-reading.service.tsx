import {
  MeterReadings,
  MeterReadingsResponse,
  EnergyUage
} from './meter-readings.interface';
import { TableColumns } from '../table/table.interface';
import { DateUtils } from '../../utils/date.service';

export class MeterReadingService {
  static readonly DATE_FORMAT = 'MMMM Do YYYY';
  static readonly columns: Array<TableColumns> = [
    {
      name: 'Date',
      dataKey: 'readingDateDisplay'
    },
    {
      name: 'Reading',
      dataKey: 'cumulative'
    },
    {
      name: 'Unit',
      dataKey: 'unit'
    }
  ];

  // TO-DO: Error handeling, based on actual API call
  // TO-DO: Convert this to async await
  // TO-DO: Use Observables if possible
  static getMeterReadings(): Promise<Array<MeterReadings>> {
    return fetch(
      'https://storage.googleapis.com/bulb-interview/meterReadingsReal.json'
    )
      .then(res => res.json())
      .then((meterReadings: MeterReadingsResponse) => {
        return meterReadings.electricity.map(value => {
          return {
            ...value,
            readingDateDisplay: DateUtils.getFormatedDate(
              value.readingDate,
              MeterReadingService.DATE_FORMAT
            )
          };
        });
      });
  }

  static predictMeterReadingAtEndOfTheMonth(
    meterReadingCurrentMonth: MeterReadings,
    meterReadingNextNomth: MeterReadings
  ) {
    if (DateUtils.isEndOfMonth(meterReadingCurrentMonth.readingDate)) {
      return {
        date: meterReadingCurrentMonth.readingDateDisplay,
        meterReading: meterReadingCurrentMonth.cumulative
      };
    }
    const usage =
      meterReadingNextNomth.cumulative - meterReadingCurrentMonth.cumulative;
    const numberOfDaysBetweenReadings =
      DateUtils.getDiffInDays(
        meterReadingNextNomth.readingDate,
        meterReadingCurrentMonth.readingDate
      ) + 1;

    const daysToEndOfMonth = DateUtils.getDaysUntilMonthEnd(
      meterReadingCurrentMonth.readingDate
    );

    const meterReadingAtEndOfMonth =
      meterReadingCurrentMonth.cumulative +
      (usage * daysToEndOfMonth) / numberOfDaysBetweenReadings;

    return {
      date: DateUtils.getFormatedDate(
        DateUtils.getLastDateOfTheMonth(meterReadingCurrentMonth.readingDate),
        MeterReadingService.DATE_FORMAT
      ),
      meterReading: meterReadingAtEndOfMonth
    };
  }

  // TO-DO: The value can be rounded of to defined decimal point
  static calculateEnergyUsageData = (
    meterReadings: Array<MeterReadings>
  ): Array<EnergyUage> => {
    const energyUsageData: Array<EnergyUage> = [];
    let previousMonthMeterReading = undefined;
    for (let i = 0; i <= meterReadings.length - 2; i++) {
      const predictedMeterReading = MeterReadingService.predictMeterReadingAtEndOfTheMonth(
        meterReadings[i],
        meterReadings[i + 1]
      );
      if (previousMonthMeterReading !== undefined) {
        energyUsageData.push({
          usage:
            predictedMeterReading['meterReading'] -
            previousMonthMeterReading.meterReading,
          date: predictedMeterReading['date']
        });
      }
      previousMonthMeterReading = predictedMeterReading;
    }
    return energyUsageData;
  };
}
