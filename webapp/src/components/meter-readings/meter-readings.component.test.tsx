import * as React from 'react';
import * as TestRenderer from 'react-test-renderer';
import { MeterReadingService } from './meter-reading.service';
import { MeterReadings } from './meter-readings.interface';
import * as moment from 'moment';
import { MeterReadingsComponent } from './meter-readings.component';

describe('<MeterReadingsComponent/>', () => {
  let data: Array<MeterReadings>;
  let mockMeterReadingData;
  beforeEach(() => {
    mockMeterReadingData = {
      electricity: [
        {
          cumulative: 18905,
          readingDate: '2017-10-27T00:00:00.000Z',
          unit: 'kWh'
        },
        {
          cumulative: 19150,
          readingDate: '2017-11-04T00:00:00.000Z',
          unit: 'kWh'
        }
      ]
    };

    data = mockMeterReadingData.electricity.map((reading: MeterReadings) => {
      return {
        ...reading,
        readingDateDisplay: moment
          .utc(reading.readingDate)
          .format(MeterReadingService.DATE_FORMAT)
      };
    });
    jest.spyOn(MeterReadingService, 'getMeterReadings').mockResolvedValue(data);
  });

  it('should render', () => {
    const tableRendrer = TestRenderer.create(<MeterReadingsComponent />);
    const tree = tableRendrer.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should call the getMeterReadings function from MeterReadingService', () => {
    TestRenderer.create(<MeterReadingsComponent />);
    expect(MeterReadingService.getMeterReadings).toHaveBeenCalledTimes(2);
  });

  describe('Table', () => {
    it('should have created a table', done => {
      const tableRendrer = TestRenderer.create(<MeterReadingsComponent />);
      setTimeout(() => {
        const root = tableRendrer.root;
        expect(root.findAllByType('table').length).toEqual(1);
        done();
      });
    });

    it('table should have two rows and one header row', done => {
      const tableRendrer = TestRenderer.create(<MeterReadingsComponent />);
      setTimeout(() => {
        const root = tableRendrer.root;
        expect(root.findAllByType('tr').length).toEqual(3);
        done();
      });
    });
  });

  describe('Graph', () => {
    it('should have svg element', done => {
      const tableRendrer = TestRenderer.create(<MeterReadingsComponent />);
      setTimeout(() => {
        const root = tableRendrer.root;
        expect(root.findAllByType('svg').length).toBeGreaterThan(0);
        done();
      });
    });
  });
});
