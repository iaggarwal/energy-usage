import {} from 'jest';
import { MeterReadingService } from './meter-reading.service';
import { MeterReadings } from './meter-readings.interface';
import * as moment from 'moment';

describe('MeterReadingService', () => {
  let MOCK_METER_READING: any;

  let MOCK_ENERGY_RESPONSE: Array<MeterReadings>;

  beforeEach(() => {
    MOCK_METER_READING = {
      electricity: [
        {
          cumulative: 18905,
          readingDate: '2017-10-27T00:00:00.000Z',
          unit: 'kWh'
        },
        {
          cumulative: 19150,
          readingDate: '2017-11-04T00:00:00.000Z',
          unit: 'kWh'
        },
        {
          cumulative: 19667,
          readingDate: '2017-12-31T00:00:00.000Z',
          unit: 'kWh'
        }
      ]
    };

    MOCK_ENERGY_RESPONSE = MOCK_METER_READING.electricity.map(
      (reading: MeterReadings) => {
        return {
          ...reading,
          readingDateDisplay: moment
            .utc(reading.readingDate)
            .format(MeterReadingService.DATE_FORMAT)
        };
      }
    );
  });
  describe('getMeterReadings', () => {
    beforeEach(() => {
      fetchMock.resetMocks();
    });

    it('should make the call to the server', () => {
      fetchMock.mockResponseOnce(JSON.stringify(MOCK_METER_READING));
      MeterReadingService.getMeterReadings();
      expect(fetchMock).toHaveBeenCalledTimes(1);
    });

    it('should make a request to url - https://storage.googleapis.com/bulb-interview/meterReadingsReal.json', () => {
      fetchMock.mockResponseOnce(JSON.stringify(MOCK_METER_READING));
      MeterReadingService.getMeterReadings();
      expect(fetchMock.mock.calls[0][0]).toEqual(
        'https://storage.googleapis.com/bulb-interview/meterReadingsReal.json'
      );
    });

    it('should return the electricity meter readings', () => {
      fetchMock.mockResponseOnce(JSON.stringify(MOCK_METER_READING));
      MeterReadingService.getMeterReadings().then(res => {
        expect(res).toEqual(MOCK_ENERGY_RESPONSE);
      });
    });
  });

  describe('predictMeterReadingAtEndOfTheMonth', () => {
    const PREDICTED_VALUE = {
      date: 'October 31st 2017',
      meterReading: 19013.88888888889
    };

    it('should return meter reading as 19013.88888888889', () => {
      const prediction = MeterReadingService.predictMeterReadingAtEndOfTheMonth(
        MOCK_ENERGY_RESPONSE[0],
        MOCK_ENERGY_RESPONSE[1]
      );
      expect(prediction.meterReading).toEqual(PREDICTED_VALUE.meterReading);
    });

    it('should return date as October 31st 2017', () => {
      const prediction = MeterReadingService.predictMeterReadingAtEndOfTheMonth(
        MOCK_ENERGY_RESPONSE[0],
        MOCK_ENERGY_RESPONSE[1]
      );
      expect(prediction.date).toEqual(PREDICTED_VALUE.date);
    });
  });

  describe('calculateEnergyUsageData', () => {
    const PREDICTED_VALUE = [
      {
        date: 'November 30th 2017',
        usage: 367.8697318007653
      }
    ];

    it('should call predictMeterReadingAtEndOfTheMonth exactly once', () => {
      jest.spyOn(MeterReadingService, 'predictMeterReadingAtEndOfTheMonth');
      MeterReadingService.calculateEnergyUsageData(MOCK_ENERGY_RESPONSE);
      expect(
        MeterReadingService.predictMeterReadingAtEndOfTheMonth
      ).toHaveBeenCalledTimes(2);
    });

    it('should return an array of length one', () => {
      const prediction = MeterReadingService.calculateEnergyUsageData(
        MOCK_ENERGY_RESPONSE
      );
      expect(prediction.length).toEqual(1);
    });

    it('should calculate energy usage as 367.8697318007653', () => {
      const prediction = MeterReadingService.calculateEnergyUsageData(
        MOCK_ENERGY_RESPONSE
      );
      expect(prediction[0].usage).toEqual(PREDICTED_VALUE[0].usage);
    });
  });
});
