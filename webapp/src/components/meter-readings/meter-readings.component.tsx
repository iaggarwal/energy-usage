import * as React from 'react';
import { MeterReadingService } from './meter-reading.service';
import { MeterReadings, State, EnergyUage } from './meter-readings.interface';
import { Table } from '../table/table.component';
import { BarChart, XAxis, YAxis, CartesianGrid, Tooltip, Bar } from 'recharts';
import {
  MeterReadingsWrapper,
  TableWrapper,
  BarChartWrapper
} from './meter-readinds.styles';

export class MeterReadingsComponent extends React.Component {
  state: State;
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      hasError: false,
      meterReadings: [],
      energyUsage: []
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    MeterReadingService.getMeterReadings().then(
      (meterReadings: Array<MeterReadings>) => {
        this.setState({
          loading: false,
          meterReadings: meterReadings,
          hasError: false,
          energyUsage: MeterReadingService.calculateEnergyUsageData(meterReadings)
        });
      }
    );
  }

  render() {
    return (
      <MeterReadingsWrapper>
        <BarChartWrapper>
          <h2>Energy Usage</h2>
          <BarChart
            className={'bar-char'}
            width={800}
            height={400}
            data={this.state.energyUsage}
          >
            <XAxis dataKey="date" />
            <YAxis dataKey="usage" />
            <CartesianGrid horizontal={false} />
            <Tooltip />
            <Bar dataKey="usage" fill="#03ad54" isAnimationActive={false} />
          </BarChart>
        </BarChartWrapper>
        <TableWrapper>
          <h2>Meter Readings</h2>
          <Table<MeterReadings>
            columns={MeterReadingService.columns}
            data={this.state.meterReadings}
            loading={this.state.loading}
          />
        </TableWrapper>
      </MeterReadingsWrapper>
    );
  }
}
