import * as React from 'react';
import { TableColumns, TableProps } from './table.interface';
import { TableStyled } from './table.styles';

export class Table<T> extends React.Component<TableProps<T>> {

  render() {
    const columns: Array<TableColumns> = this.props.columns;
    const data: Array<T> = this.props.data;
    const loading: boolean = this.props.loading;

    if (loading) {
      return <div>Loading table data....</div>;
    } else {
      const header = this.getHeader(columns);
      const body = this.getBody(data, columns);
      return (
        <TableStyled>
          {header}
          {body}
        </TableStyled>
      );
    }
  }

  private getHeader = (columns: Array<TableColumns>) => {
    return (
      <thead>
        <tr>
          {columns.map(col => (
            <th key={col.dataKey}>{col.name}</th>
          ))}
        </tr>
      </thead>
    );
  };

  private getBody = (
    data: Array<T>,
    columns: Array<TableColumns>
  ) => {
    const tableRows = data.map((reading: T, index: number) => {
      return (
        <tr key={index}>
          {columns.map((col, index) => (
            <td key={index}> {reading[col.dataKey]} </td>
          ))}
        </tr>
      );
    });
    return <tbody>{tableRows}</tbody>;
  };
}
