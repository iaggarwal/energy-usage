export interface TableColumns {
    name: string;
    dataKey: string;
}

export interface TableProps<T> {
    columns: Array<TableColumns>;
    data: Array<T>;
    loading: boolean;
}
