import styled from 'styled-components';

export const TableStyled = styled.table`
    pading: 5px;
    border-spacing: 0;

    tr:nth-of-type(2n) {
        background-color: #F2F2F2;
    }

    td {
        height: 3vh;
    }
`