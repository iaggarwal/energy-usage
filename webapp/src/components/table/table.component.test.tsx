import * as React from 'react';
import * as TestRenderer from 'react-test-renderer';
import { Table } from './table.component';
import { TableColumns } from './table.interface';
import { MeterReadingService } from '../meter-readings/meter-reading.service';
import { MeterReadings } from '../meter-readings/meter-readings.interface';
import * as moment from 'moment';

fdescribe('<Table/>', () => {
  let tableRendrer: any;
  let columns: Array<TableColumns>;
  let data: Array<MeterReadings>;
  let mockMeterReadingData;

  beforeEach(() => {
    columns = MeterReadingService.columns;
    mockMeterReadingData = {
      electricity: [
        {
          cumulative: 18905,
          readingDate: '2017-10-27T00:00:00.000Z',
          unit: 'kWh'
        },
        {
          cumulative: 19150,
          readingDate: '2017-11-04T00:00:00.000Z',
          unit: 'kWh'
        }
      ]
    };

    data = mockMeterReadingData.electricity.map((reading: MeterReadings) => {
      return {
        ...reading,
        readingDateDisplay: moment
          .utc(reading.readingDate)
          .format(MeterReadingService.DATE_FORMAT)
      };
    });
  });

  describe('Loading is true', () => {
    it('renders correctly', () => {
      tableRendrer = TestRenderer.create(
        <Table<MeterReadings> columns={columns} data={data} loading={true} />
      );
      const tree = tableRendrer.toJSON();
      expect(tree).toMatchSnapshot();
    });

    it('should show show a div', () => {
      tableRendrer = TestRenderer.create(
        <Table<MeterReadings> columns={columns} data={data} loading={true} />
      );
      const root = tableRendrer.root;
      expect(root.findAllByType('div').length).toEqual(1);
    });
  });

  describe('Loading is false', () => {
    it('renders correctly', () => {
      tableRendrer = TestRenderer.create(
        <Table<MeterReadings> columns={columns} data={data} loading={false} />
      );
      const tree = tableRendrer.toJSON();
      expect(tree).toMatchSnapshot();
    });

    it('should have a table element', () => {
      tableRendrer = TestRenderer.create(
        <Table<MeterReadings> columns={columns} data={data} loading={false} />
      );
      const root = tableRendrer.root;
      expect(root.findAllByType('table').length).toEqual(1);
    });

    it('should have a as many <th> as number of columns', () => {
      tableRendrer = TestRenderer.create(
        <Table<MeterReadings> columns={columns} data={data} loading={false} />
      );
      const root = tableRendrer.root;
      expect(root.findAllByType('th').length).toEqual(columns.length);
    });

    it('should have a as many <td> as number of columns multiplied by number of data elements', () => {
      tableRendrer = TestRenderer.create(
        <Table<MeterReadings> columns={columns} data={data} loading={false} />
      );
      const root = tableRendrer.root;
      expect(root.findAllByType('td').length).toEqual(columns.length * (data.length));
    });
  });
});
