import styled from 'styled-components';

export const Card = styled.div`
    border-radius: 5px;
    padding: 10px;
`