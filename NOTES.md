Candidate Name: Ishan Aggarwal

Tasks: 1 and 3

Time: 6 Hours

Notes:
    Approach: We want to use the data to create a table and a graph. The data used to rended the two is related, which suggests that the two should share the same parent. As such the state should also be handled in the parent component, while the children components deal only with the props.
    Table component for the purpose of this exercise is rather simple one. I have used the HTML table. I have made the component generic enough for it to handle different sorts of data and is  not ties to the data for this exercise.
    Styling of the wrapper components for graph and the table should be similar and hence both of them inherit the same styles. I have defined a card style, which essentially gives each of the wrappers some visual aid. Each of them can have their own styles as well.
    I am using the loading property on the state only for the table component, which can easily be extended to handle the graph rendering. The idea is to show something while the data is loaded (Spinner would be ideal).

    Even though I had heard about the styled components, this is the first time I used them, and I have mixed feelings about it. I like the was it makes life easy for a developer, but has a unique learning curve. But I thoroughly enjoyed using it.
