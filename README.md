## Commands

For `webapp`,

* `yarn install` - install all dependencies
* `yarn build` - build the code
* `yarn start` - start a developer web server
* `yarn test` - run all unit tests

For `server`,

* `yarn install` - install all dependencies
* `yarn build` - compile the TypeScript to JavaScript
* `yarn start` - compile and run the web server, watching for updates and restarting as required
* `yarn serve` - start the web server using the compiled JS. The code must be compiled first. Useful for running in production.
* `yarn test` - run all unit tests

If you get a port conflict when running the server use the `PORT` environment variable to change it, e.g. `PORT=3001 yarn start` or in Windows:

```
set PORT=3001
yarn start
```
